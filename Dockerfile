FROM node:8

# Install yarn
RUN \
  wget -q -O - https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
  echo "deb http://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list && \
  apt-get update && \
  apt-get install -y yarn

# Install Chromium & Xvfb
RUN \
  wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
  echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list && \
  apt-get update && \
  apt-get install -y google-chrome-stable xvfb && \
  rm -rf /var/lib/apt/lists/* 

# Upgrade NPM to latest (address issue #3)
RUN npm install -g npm

# Install Protractor
RUN npm install -g protractor

# Install Selenium and Chrome driver
RUN webdriver-manager update

WORKDIR 'usr/src/demop' 
COPY . .
CMD [ "protractor", "conf.js" ]