// conf.js
exports.config = {
    framework: 'jasmine',
    useAllAngular2AppRoots: true,
    // seleniumAddress: 'http://localhost:32782/wd/hub',
    seleniumAddress: 'http://selenium-hub:4444/wd/hub',
    specs: ['spec.js']
  }